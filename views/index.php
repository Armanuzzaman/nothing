<?php
session_start();
include_once('../vendor/autoload.php');
use App\Hobby\Hobby;

$object=new Hobby();

$all=$object->index();
//print_r($all);
?>
<html>
<head>
    <title>
        index
    </title>
</head>
<body>
<a href="create.php" role="button">Create New</a>
<h1>Index page</h1>
<table>
    <thead>
    <tr>
        <th>Serial</th>
        <th>ID</th>
        <th>Hobbies</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $count =0 ;
    foreach ($all as $item) { $count++;
        ?>
        <tr>


            <td><?php echo $count?> </td>
            <td> <?php echo $item->id ?> </td>
            <td> <?php echo $item->hobby ?> </td>
            <td>
                <a href="view.php?id=<?php echo $item->id?>"  role="button">View</a>
                <a href="edit.php?id=<?php echo $item->id?>"  role="button">Update</a>
                <a href="trash.php?id=<?php echo $item->id?>" role="button">Trash</a>
                <a href="delete.php?id=<?php echo $item->id?>" role="button" id="delete"  Onclick="return ConfirmDelete()">Delete</a>
            </td>
        </tr>
    <?php } ?>
    </tbody>
</table>
</body>
</html>