<?php
namespace App\Hobby;

class Hobby
{
    public $id ="";
    public $hobby="";
    public $conn="";
    public $deleted_at;

    public function __construct()
    {
        $this->conn=mysqli_connect("localhost","root","","practice") or die("connection failed");
    }

    public function prepare($data){
        if(array_key_exists("id",$data))
        {
            $this->id=$data['id'];
        }
        if(array_key_exists("hobby",$data)){
            $this->hobby=$data['hobby'];
        }

    }
    public function store(){
        $query="INSERT INTO `practice`.`hobbies` (`hobby`) VALUES ('".$this->hobby."')";
        //echo $query;
        $result=mysqli_query($this->conn,$query);
        //echo $result;

        if($result){
            echo "Data stored";
        }
        else{
            echo "storing failed";
        }
    }
    public function index(){
        $all=array();
        $query="SELECT * FROM `hobbies` WHERE `deleted_at` IS NULL ";
        $result=mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_object($result)){
            $all[]=$row;
        }
        return $all;
    }

    public function trash(){
        $this->deleted_at=time();
        $query="UPDATE `hobbies` SET `deleted_at`=".$this->deleted_at." WHERE `id`=".$this->id;
        //echo $query;

        $result=mysqli_query($this->conn,$query);
        if($result){
            echo "data has been trashed";
        }
        else{
            echo "something wrong";
        }

    }
    public function trashed(){
        $allT=array();
        $query="SELECT * FROM `hobbies` WHERE `deleted_at` IS NOT NULL";
        $result=mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_object($result)){
            $allT[]=$row;
        }
        return $allT;
    }

    public function recoverselected($IDs= array()){
        if(is_array($IDs) && count($IDs)>0){
            $ids = implode(",",$IDs);
            echo $ids;
            //$query = "UPDATE `hobbies` SET `deleted_at`= NULL WHERE `hobbies`.`id` IN (".$ids.")";
            //$result = mysqli_query($this->conn, $query);
            /*if($result){
                Message::message("Data has been recovered successfully");
                Utility::redirect("index.php");
            }
            else
                echo "ERROR!";*/
        }

    }

}